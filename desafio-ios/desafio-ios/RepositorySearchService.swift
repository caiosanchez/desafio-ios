//
//  RepositorySearchService.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 10/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation

enum SearchParameter {
    
    case language(name: String)
    case order(OrderType)
    case page(number: Int)
    case sort(SortType)
    
    func buildParameter() -> String {
        switch self {
        case .language(let name):
            return "language:\(name)"
        case .order(let order):
            return "order:\(order.rawValue)"
        case .page(let pageNumber):
            return "page:\(pageNumber)"
        case .sort(let sortType):
            return "sort:\(sortType)"
        }
    }
}

extension SearchParameter: Equatable {
    static func ==(lhs: SearchParameter, rhs:  SearchParameter) -> Bool{
        switch (lhs, rhs) {
        case (let .language(value1), let .language(value2)):
            return value1 == value2
        case (let .order(value1), let .order(value2)):
            return value1 == value2
        case (let .page(value1), let .page(value2)):
            return value1 == value2
        case (let .sort(value1), let .sort(value2)):
            return value1 == value2
        default:
            return false
        }
    }
}

enum OrderType: String {
    case ascending = "asc"
    case descending = "desc"
}

enum SortType: String {
    case stars
    case forks
    case updated
}

class RepositorySearchService: Service {
    
    typealias EntityType = RepositorySearchResult
    
    func searchRepositories(parameters: [SearchParameter],
                            completionHandler: @escaping (RepositorySearchResult?, Error?) -> Void) {
        let url = buildURL(parameters: parameters)
        self.requestObject(url: url, completionHandler)
    }
    
    private func buildURL(parameters: [SearchParameter]) -> String {
        var url = Config.gitHubBaseURL + "/search/repositories?q="
        for parameter in parameters {
            url += parameter.buildParameter()
            if (parameters.last != parameter) {
                url += "&"
            }
        }
        return url
    }

    
}
