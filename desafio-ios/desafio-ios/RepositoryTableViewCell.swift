//
//  RepositoryTableViewCell.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 10/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import UIKit
import Kingfisher

class RepositoryTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "repositoryCell"
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var starsCountLabel: UILabel!
    @IBOutlet weak var forksCountLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.clipsToBounds = true
        avatarImageView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fill(with repository: Repository) {
        nameLabel.text = repository.name
        descriptionLabel.text = repository.description
        starsCountLabel.text = "\(repository.starsCount)"
        forksCountLabel.text = "\(repository.forksCount)"
        usernameLabel.text = repository.owner.login
        loadImage(forRepositoryOwner: repository.owner)
    }
    
    func loadImage(forRepositoryOwner owner: User) {
        avatarImageView.kf.setImage(with: owner.avatarURL, options: [.transition(.fade(1))])
      
    }

}
