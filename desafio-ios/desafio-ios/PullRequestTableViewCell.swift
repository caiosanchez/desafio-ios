//
//  PullRequestTableViewCell.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 12/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import UIKit
import Kingfisher

class PullRequestTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "pullRequestCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        authorImageView.clipsToBounds = true
        authorImageView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    func fill(with pullRequest: PullRequest) {
        titleLabel.text = pullRequest.title
        bodyLabel.text = pullRequest.body
        authorNameLabel.text = pullRequest.author.login
        createdAtLabel.text = pullRequest.createdAt.toString(format: Constants.PullRequestCell.dateFormat)
        loadImage(forAuthor: pullRequest.author)
    }
    
    func loadImage(forAuthor owner: User) {
        authorImageView.kf.setImage(with: owner.avatarURL, options: [.transition(.fade(1))])
    }

}
