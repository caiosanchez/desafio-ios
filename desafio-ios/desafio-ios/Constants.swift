//
//  Constants.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 11/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation

struct Constants {
    
    /** The number of cells before reach the bottom of the tableView that will trigger
     a new request for more data
    */
    static let cellIndexOffSetForFetchingNewValues = 5
    
    struct PullRequestCell {
        static let dateFormat = "dd/MM/YYYY HH:mm"
    }
    
    
}
