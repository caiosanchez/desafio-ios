//
//  GitHubUser.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 09/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation
import ObjectMapper

struct User {
    
    var login: String!
    var avatarURL: URL?
    
}

extension User: Mappable {
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        avatarURL   <- (map["avatar_url"], URLTransform())
        login       <- map["login"]
    }
}

