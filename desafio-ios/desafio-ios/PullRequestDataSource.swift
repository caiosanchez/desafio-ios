//
//  PullRequestDataSource.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 12/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation
import UIKit

protocol PullRequestDataSourceDelegate {
    func dataUpdated()
    func fetchReturnedWithNoResults()
    func fetchReturned(withError error: Error)
}

class PullRequestDataSource: NSObject {
    
    var delegate: PullRequestDataSourceDelegate?
    var pullRequests = [PullRequest]()
    fileprivate var repositry: Repository
    fileprivate let pullRequestService = PullRequestService()
    
    init(repository: Repository) {
        self.repositry = repository
        super.init()
        fetchPullRequests(for: repository)
    }
    
    func fetchPullRequests(for repository: Repository) {
        pullRequestService.requestPullRequests(for: repository) { pullRequests, error in
            guard error == nil else {
                self.delegate?.fetchReturned(withError: error!)
                return
            }
            if pullRequests!.count > 0 {
                self.pullRequests.append(contentsOf: pullRequests!)
                self.delegate?.dataUpdated()
            } else {
                self.delegate?.fetchReturnedWithNoResults()
            }
           
        }
    }
    
}

extension PullRequestDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = PullRequestTableViewCell.cellIdentifier
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PullRequestTableViewCell else {
            fatalError("Could not dequeue PullRequestTableViewCell")
        }
        let pullRequest = pullRequests[indexPath.row]
        cell.fill(with: pullRequest)
        return cell
    }
    
}

