//
//  RepositorySearchResult.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 09/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation
import ObjectMapper

struct RepositorySearchResult {
    
    var items: [Repository] = []
    var totalCount: Int = 0
    
    subscript(index: Int) -> Repository {
        return items[index]
    }
}

extension RepositorySearchResult: Mappable {
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        items       <- map["items"]
        totalCount  <- map["total_count"]
    }
    
}

