//
//  WebViewController.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 12/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    var url: URL?
    
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = url {
            loadURL(url)
            self.navigationItem.prompt = url.absoluteString
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadURL(_ url: URL) {
        let urlRequest = URLRequest(url: url)
        webView.loadRequest(urlRequest)
    }

}
