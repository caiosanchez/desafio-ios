//
//  PullRequestTableViewController.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 12/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import UIKit

class PullRequestTableViewController: UITableViewController {
    
    var repository: Repository!
    var pullRequestDataSouce: PullRequestDataSource!
    fileprivate var initialLoadingView: LoadingView?


    override func viewDidLoad() {
        super.viewDidLoad()
        showInitialActivityIndicator()
        pullRequestDataSouce = PullRequestDataSource(repository: repository)
        tableView.dataSource = pullRequestDataSouce
        pullRequestDataSouce.delegate = self
        setupSelfSizingCell()
        self.title = repository.name
    }
    
    fileprivate func showInitialActivityIndicator() {
        initialLoadingView = LoadingView(frame: tableView.frame)
        initialLoadingView?.autoresizingMask = tableView.autoresizingMask
        tableView.addSubview(initialLoadingView!)
        tableView.separatorStyle = .none
        
    }
    
    fileprivate func removeInitialActivityIndicator() {
        initialLoadingView?.removeFromSuperview()
        initialLoadingView?.activityIndicator.stopAnimating()
        initialLoadingView = nil
        tableView.separatorStyle = .singleLine
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    fileprivate func setupSelfSizingCell() {
        tableView.estimatedRowHeight = 110
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedCellIndex = tableView.indexPathForSelectedRow {
            let pullRequestURL = pullRequestDataSouce.pullRequests[selectedCellIndex.row].url
            let webViewController = segue.destination as! WebViewController
            webViewController.url = pullRequestURL
        }
    }
}


extension PullRequestTableViewController: PullRequestDataSourceDelegate {
    
    func dataUpdated() {
        tableView.reloadData()
        removeInitialActivityIndicator()
    }
    
    func fetchReturnedWithNoResults() {
        let emptyResultView = EmptyResultView(frame: tableView.frame)
        emptyResultView.messageLabel.text = "No pull requests =("
        emptyResultView.autoresizingMask = tableView.autoresizingMask
        self.tableView.addSubview(emptyResultView)
    }
    
    func fetchReturned(withError error: Error) {
        
    }
}
