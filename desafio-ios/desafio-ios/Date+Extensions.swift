//
//  Date+Extensions.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 12/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation

extension Date {
    
    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}
