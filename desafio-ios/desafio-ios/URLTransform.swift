//
//  URLTransform.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 10/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation
import ObjectMapper

class URLTransform: TransformType {
    typealias Object = URL
    typealias JSON = String
    
    func transformFromJSON(_ value: Any?) -> URL? {
        
        guard let stringURL = value as? String else { return nil }
        return URL(string: stringURL)
    }
    
    func transformToJSON(_ value: URL?) -> String? {
        guard let url = value else { return nil }
        return url.absoluteString
    }
}
