//
//  RepositoryTableViewController.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 10/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import UIKit

class RepositoryTableViewController: UITableViewController {
    
    fileprivate var dataSource = RepositoryDataSource()
    fileprivate var bottomActivityIndicator: UIActivityIndicatorView!
    fileprivate var initialLoadingView: LoadingView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = dataSource
        showInitialActivityIndicator()
        dataSource.delegate = self
        setupSelfSizingCell()
        setRefreshControlView()
        self.clearsSelectionOnViewWillAppear = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func showInitialActivityIndicator() {
        initialLoadingView = LoadingView(frame: tableView.frame)
        initialLoadingView?.autoresizingMask = tableView.autoresizingMask
        tableView.addSubview(initialLoadingView!)
        tableView.separatorStyle = .none
  
    }
    
    fileprivate func removeInitialActivityIndicator() {
        initialLoadingView?.removeFromSuperview()
        initialLoadingView?.activityIndicator.stopAnimating()
        initialLoadingView = nil
        tableView.separatorStyle = .singleLine
    }
    
    fileprivate func setupSelfSizingCell() {
        tableView.estimatedRowHeight = 110
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    fileprivate func setRefreshControlView() {
        let frame = CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 44)
        bottomActivityIndicator = UIActivityIndicatorView(frame: frame)
        bottomActivityIndicator.activityIndicatorViewStyle = .gray
    }
    
    fileprivate func showBottomActivityIndicator() {
        self.tableView.tableFooterView = bottomActivityIndicator
        bottomActivityIndicator.startAnimating()
    }
    
    fileprivate func hideBottomRefreshControl() {
        bottomActivityIndicator.stopAnimating()
        tableView.tableFooterView = nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedCellIndex = tableView.indexPathForSelectedRow {
            let repository = dataSource.repositories[selectedCellIndex.row]
            let pullRequestTableViewController = segue.destination as! PullRequestTableViewController
            pullRequestTableViewController.repository = repository
        }
    }

}

//MARK: TableViewDelegate
extension RepositoryTableViewController {
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (dataSource.numberOfItems - Constants.cellIndexOffSetForFetchingNewValues) {
            showBottomActivityIndicator()
            dataSource.fetchNextPage()
        }
    }
    
}

extension RepositoryTableViewController: RepositoryDataSourceDelegate {
    
    func dataUpdated() {
        tableView.reloadData()
        removeInitialActivityIndicator()
        hideBottomRefreshControl()
    }
    
    func searchReturnedWithNoResult() {
        let emptyResultView = EmptyResultView(frame: tableView.frame)
        emptyResultView.messageLabel.text = "No results =("
        emptyResultView.autoresizingMask = tableView.autoresizingMask
        self.tableView.addSubview(emptyResultView)
    }
    
    func searchReturned(withError error: Error) {
        
    }
    
}


