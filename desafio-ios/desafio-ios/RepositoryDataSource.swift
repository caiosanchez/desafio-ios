//
//  RepositoryDataSource.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 10/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation
import UIKit

protocol RepositoryDataSourceDelegate {
    func dataUpdated()
    func searchReturnedWithNoResult()
    func searchReturned(withError error: Error)
}

class RepositoryDataSource: NSObject {
    
    var delegate: RepositoryDataSourceDelegate?
    var numberOfItems: Int {
        return repositories.count
    }
    
    var repositories = [Repository]()
    fileprivate let repositorySearchService = RepositorySearchService()
    fileprivate var currentPageNumber = 1
    
    fileprivate var defaultParameters: [SearchParameter] {
        let language = SearchParameter.language(name: "Java")
        let sort = SearchParameter.sort(.stars)
        let order = SearchParameter.order(.descending)
        let paramenters = [language, sort, order]
        return paramenters
    }
    
    override init() {
        super.init()
        fetchRepositories(forPage: currentPageNumber)
    }
    
    fileprivate func fetchRepositories(forPage number: Int) {
        var parameters = defaultParameters
        parameters.append(SearchParameter.page(number: number))
        repositorySearchService.searchRepositories(parameters: parameters) { (searchResult, error) in
            guard error == nil else {
                self.delegate?.searchReturned(withError: error!)
                return
            }
            if searchResult!.items.count > 0 {
                self.append(contentOf: searchResult!)
                self.delegate?.dataUpdated()
            } else {
                self.delegate?.searchReturnedWithNoResult()
            }
        }
    }
    
    fileprivate func append(contentOf searchResult: RepositorySearchResult) {
        self.repositories.append(contentsOf: searchResult.items)
    }
    
    
    func fetchNextPage() {
        currentPageNumber += 1
        fetchRepositories(forPage: currentPageNumber)
    }
    
}

extension RepositoryDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = RepositoryTableViewCell.cellIdentifier
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? RepositoryTableViewCell else {
                fatalError("Could not dequeue repositoryCell")
        }
        let repository = repositories[indexPath.row]
        cell.fill(with: repository)
        return cell
    }
    
}
