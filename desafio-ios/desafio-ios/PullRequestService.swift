//
//  PullRequestService.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 12/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation

class PullRequestService: Service {
    
    typealias EntityType = PullRequest
    
    func requestPullRequests(for repository: Repository, completionHandler: @escaping ([PullRequest]?, Error?) -> Void) {
        let url = buildPullRequestURL(for: repository)
        requestList(url: url, completionHandler)
    }
    
    fileprivate  func buildPullRequestURL(for repository: Repository) -> String {
        return Config.gitHubBaseURL + "/repos/\(repository.owner.login!)/\(repository.name!)/pulls"
    }
    
}
