//
//  Service.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 10/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


protocol Service {
    
    associatedtype EntityType
    
}

extension Service where EntityType: Mappable {
    
    func requestList(url: String, _ completionHandler: @escaping ([EntityType]?, Error?) -> Void) {
        Alamofire.request(url).responseArray { (response: DataResponse<[EntityType]>) in
            switch response.result {
            case .failure(let error):
                completionHandler(nil, error)
            case .success(let result):
                completionHandler(result, nil)
            }
        }
    }
    
    func requestObject(url: String, _ completionHandler: @escaping (EntityType?, Error?) -> Void) {
        Alamofire.request(url).responseObject { (response: DataResponse<EntityType>) in
            switch response.result {
            case .failure(let error):
                completionHandler(nil, error)
            case .success(let result):
                completionHandler(result, nil)
            }
        }
    }
    
}
