//
//  GitHubPullRequest.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 09/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation
import ObjectMapper

struct PullRequest {
    
    var author: User!
    var body: String!
    var createdAt: Date!
    var repository: Repository!
    var title: String!
    var url: URL!
    
}

extension PullRequest: Mappable {
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        author      <- map["user"]
        body        <- map["body"]
        createdAt   <- (map["created_at"], ISO8601DateTransform())
        repository  <- map["repo"]
        title       <- map["title"]
        url         <- (map["html_url"], URLTransform())
    }
}
