//
//  Config.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 10/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation

struct Config {
   
    static let gitHubBaseURL = "https://api.github.com"
    
}
