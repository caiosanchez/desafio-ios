//
//  GitHubRepository.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 09/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation
import ObjectMapper

struct Repository {
    
    var description: String!
    var forksCount: Int = 0
    var name: String!
    var owner: User = User()
    var starsCount: Int = 0
    
}

extension Repository: Mappable {
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        description <- map["description"]
        forksCount  <- map["forks_count"]
        name        <- map["name"]
        starsCount  <- map["stargazers_count"]
        owner       <- map["owner"]
    }

}



