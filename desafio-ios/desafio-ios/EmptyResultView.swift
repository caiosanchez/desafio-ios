//
//  EmptyResultView.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 12/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import UIKit

class EmptyResultView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commomInit()
    }
    
    private func commomInit() {
        Bundle.main.loadNibNamed("EmptyResultView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        
    }

}
