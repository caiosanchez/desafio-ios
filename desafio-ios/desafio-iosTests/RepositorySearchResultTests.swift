//
//  RepositorySearchResultTests.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 09/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import XCTest
@testable import desafio_ios

class RepositorySearchResultTests: XCTestCase {
    
    var jsonLoader: JSONLoader!
    
    override func setUp() {
        super.setUp()
        jsonLoader = JSONLoader()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testJSONParse() {
        do {
            if let jsonObject = try jsonLoader.loadRepositorySearchResultJSON() {
                let searchResult = RepositorySearchResult(JSON: jsonObject)
                XCTAssertNotNil(searchResult)
                XCTAssertTrue(searchResult?.items.count == 30)
                XCTAssertNotNil(searchResult?.items.first?.owner)
            } else {
                XCTFail("Could not load local repository search result json file.")
            }
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
}
