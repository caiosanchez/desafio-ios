//
//  JSONLoader.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 09/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import Foundation

class JSONLoader {
    
    func load(forResource resource: String) throws -> Any? {
        let bundle = Bundle(for: type(of: self))
        guard let filePath = bundle.path(forResource: resource, ofType: "json") else {
            return nil
        }
        let data = try Data(contentsOf: URL(fileURLWithPath:filePath), options: .uncached)
        let object = try JSONSerialization.jsonObject(with: data, options: [])
        return object
    }
    
    func loadRepositoryJSON() throws -> [String: Any]? {
        return try load(forResource: "repository") as? [String : Any]
    }
    
    func loadUserJSON() throws -> [String: Any]?  {
        return try load(forResource: "user") as? [String : Any]
    }
    
    func loadPullRequestJSON() throws -> [String: Any]?  {
        return try load(forResource: "pullRequest") as? [String : Any]
    }
    
    func loadRepositorySearchResultJSON() throws -> [String: Any]?  {
        return try load(forResource: "searchResult") as? [String : Any]
    }
    
}
