//
//  RepositoryTests.swift
//  desafio-ios
//
//  Created by Caio Sanchez Santos Costa on 09/06/17.
//  Copyright © 2017 Caio Sanchez Santos Costa. All rights reserved.
//

import XCTest
@testable import desafio_ios

class RepositoryTests: XCTestCase {
    
    var jsonLoader: JSONLoader!
    
    override func setUp() {
        super.setUp()
        jsonLoader = JSONLoader()
    }
    
    override func tearDown() {
        super.tearDown()
        jsonLoader = nil
    }
    
    func testJSONParse() {
        do {
            if let jsonObject = try jsonLoader.loadRepositoryJSON() {
                let repository = Repository(JSON: jsonObject)
                XCTAssertNotNil(repository)
                let owner = repository?.owner
                XCTAssertNotNil(owner)
            } else {
                XCTFail("Could not load local repository json file.")
            }
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
}
